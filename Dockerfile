# Use an official Python runtime as a parent image
FROM python:3.8-slim-buster

# Install huggingface-cli and hf-transfer
RUN pip install --upgrade pip && \
    pip install -U "huggingface_hub[cli]" hf-transfer

# Set the environment variable
ENV HF_HUB_ENABLE_HF_TRANSFER=1

# Run huggingface-cli to download the model
CMD ["sh", "-c", "huggingface-cli download $MODEL_NAME"]